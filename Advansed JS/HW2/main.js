const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

// const htmlContent = books.map((item) => {
//   let htmlText = `<li>{autor}
// 	{name}
// 	{price}</li>`;

//   if (item.author) {
// 	htmlText = htmlText.replace('{autor}',`<div>Autor Book : ${item.author}</div>`);
//   }else{
// 	htmlText = htmlText.replace('{autor}',``);
// 	console.log("властивості autor немає в об'єкті")
//   }
//   if(item.name){
// 	htmlText = htmlText.replace('{name}', `<div>Name : ${item.name}</div>`)
//   }else{
// 	htmlText = htmlText.repeat('{name}', ``)
// 	console.log("властивості name немає в об'єкті")
//   }
//   if(item.price){
// 	htmlText = htmlText.replace('{price}', `<div>Price : ${item.price}</div>`)
//   }else{
// 	htmlText = htmlText.repeat('{price}', ``)
// 	console.log("властивості price немає в об'єкті")
//   }

//   return htmlText;
// }).join('');




//---------------------------------------------------------------------------------------------------------------//

// const htmlContent = books.map((item) => {
// 	let author = item.author ? `<div>Autor Book : ${item.author}</div>`: "";
// 	let name = item.name ? `<div>Name : ${item.name}</div>`: "";
// 	let price = item.price ? `<div>Price : ${item.price}</div>`: "";
// 	let htmlText = `${author} ${name} ${price}`;

// 	return htmlText;
//  }).join('');

// const htmlContent = books
//   .map((item) => {
//     return `${item.author ? `<div>Autor Book : ${item.author}</div>` : ""} ${item.name ? `<div>Name : ${item.name}</div>` : ""} ${item.price ? `<div>Price : ${item.price}</div>` : ""}`;
//   })
//   .join("");

//---------------------------------------------------------------------------------------------------------------//


// const root = document.createElement("div");
// root.id = "root";
// const list = document.createElement("ul");
// //list.innerHTML = htmlContent;

// root.append(list);
// document.body.append(root);


// for(const book of books){
// 	let listItem = document.createElement('li');
// 	list.append(listItem);
// 	if(book.author){
// 		let author = document.createElement('div');
// 		listItem.append(author);
// 		author.textContent = `Autor Book : ${book.author}`;
// 	}
// 	if(book.name){
// 		let name = document.createElement('div');
// 		listItem.append(name);
// 		name.textContent = `Name : ${book.name}`;
// 	}

// 	if(book.price){
// 		let price = document.createElement('div');
// 		listItem.append(price);
// 		price.textContent = `Price : ${book.price}`;
// 	}
// }

const root = document.createElement("div");
root.id = "root";
const list = document.createElement("ul");
//list.innerHTML = htmlContent;

root.append(list);
document.body.append(root);


for(const book of books){
	let listItem = document.createElement('li');
	list.append(listItem);
	try{
		if(book.author){
		let author = document.createElement('div');
		listItem.append(author);
		author.textContent = `Autor Book : ${book.author}`;
		}else{
			throw new Error('autor отсутствует'); 
		}
	}
	catch(error){
		console.log(error.message)
	}

	try{
		if(book.hasOwnProperty('name')){
		let name = document.createElement('div');
		listItem.append(name);
		name.textContent = ` Name : ${book.name}`;
		}else{
			throw new Error('name отсутствует'); 
		}
	}
	catch(error){
		console.log(error.message);
	}

	try{
		if('price' in book){
		let price = document.createElement('div');
		listItem.append(price);
		price.textContent = ` Price : ${book.price}`;
		}else{
			throw new Error('price отсутствует'); 
		}
	}
	catch(error){
		console.log(error.message);
	}

}