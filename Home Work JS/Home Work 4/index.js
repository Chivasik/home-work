// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено 
// +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

const result = prompt("Какую математическую операцию нужно провести?", 'Например ( + ) ( - ) ( * ) ( / )');
const firstNumber = +prompt("Введите первое число");
const secondNumber = +prompt("Введите второе число");

const getResult = function (firstNumber, secondNumber, result) {
	
	if(result === '+') return firstNumber + secondNumber;
	if(result === '-') return firstNumber - secondNumber;
	if(result === '*') return firstNumber * secondNumber;
	if(result === '/') return firstNumber / secondNumber;

	return ("Вы не ввели математическую операцию");

}

console.log(getResult(firstNumber, secondNumber, result));