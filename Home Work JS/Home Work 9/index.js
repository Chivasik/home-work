// Завдання
// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:

// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.
// Необов'язкове завдання підвищеної складності:
// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:

// ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки



const testArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const displayArrElementsOnPage = (arr, container = document.body)=>{

	const listElem = document.createElement('ul')

	arr.forEach(element => {
		const newElem = document.createElement('li')
		
		if(Array.isArray(element)){
			const listElem1 = document.createElement('ul')
			element.forEach(elem => {
				const newElem1 = document.createElement('li')
				newElem1.innerHTML = elem
				listElem1.append(newElem1)
			})
			newElem.append(listElem1)
		}else{
			newElem.innerHTML = element
		
		}
		listElem.append(newElem)

	});
	container.append(listElem)

}


displayArrElementsOnPage(arr)
