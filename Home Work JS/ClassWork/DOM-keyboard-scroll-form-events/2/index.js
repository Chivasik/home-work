// 1. При положенні скролла сторінки більше 100px додавати до хедеру стилі для тіні (`class="shadow"`). При положенні скролла менше 100px тіні не повинно бути.
// 2. При положенні скролл сторінки більше висоти показувати кнопку scrollToTop. При менше – приховувати.
// 3. При натисканні на кнопку - плавно скролити до початку документа.


const header = document.querySelector('.header')
const button = document.getElementById('scroll-to-top')
window.addEventListener('scroll', event => {
	console.log(window.scrollY)
	console.log(window.innerHeight)
	if(window.scrollY > 100) {
		header.classList.add('shadow')
	}else{
		header.classList.remove('shadow')
	}
	if(window.scrollY > window.innerHeight){
		button.style.display = 'block'
	}else{
		button.style.display = 'none'
	}
})

button.addEventListener('click', ()=>{

	window.scrollTo({
		top: 0,
		left: 0,
		behavior: 'smooth'
	})
})
