/**
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */

const counter = document.getElementById('counter')
const incrementBtn = document.getElementById('increment-btn')
const decrementBtn = document.getElementById('decrement-btn')
let additionalCounter = 0;

incrementBtn.addEventListener('click', event =>{

	counter.innerHTML = `Counter: ${++additionalCounter}`
})

decrementBtn.addEventListener('click', event =>{

	counter.innerHTML = `Counter: ${--additionalCounter}`
})