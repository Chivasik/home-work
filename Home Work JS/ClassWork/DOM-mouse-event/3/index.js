const PHRASE = 'Hello Worsssld';
const title = document.createElement("h1");
const wordsArray = PHRASE.split("").map(elem => {
	return `<span>${elem}</span>`
});
title.innerHTML = wordsArray.join('');
console.log(wordsArray);

const btn = document.createElement("button");

btn.innerText = "Paint";

btn.addEventListener('click', () => {
	for (let span of title.children){
		span.style.color = getRandomColor();
	}
});


// document.body.addEventListener('mousemove', () => {
// 	for (let span of title.children){
// 		span.style.color = getRandomColor();
// 	}
// });



document.body.append(title, btn);

function getRandomColor() {
	const r = Math.floor(Math.random() * 255);
	const g = Math.floor(Math.random() * 255);
	const b = Math.floor(Math.random() * 255);
 
	return `rgb(${r}, ${g}, ${b})`;
 }
 

// const PHRASE1 = 'Слава Україні!';
// const title1 = document.createElement('h1');
// const wordsArray1 = PHRASE1.split("").map(elem => {
//   return `<span>${elem}</span>`
// });
// title1.innerHTML = wordsArray1.join('');
// console.log(wordsArray1)
// const btn1 = document.createElement('button');
// btn1.innerText = 'Розфарбувати';
// btn1.addEventListener('click', () => {
//   for(let span of title1.children) {
//     span.style.color = getRandomColor();
//   }  
// })

// document.body.append(title1, btn1);

// function getRandomColor() {
//   const r = Math.floor(Math.random() * 255);
//   const g = Math.floor(Math.random() * 255);
//   const b = Math.floor(Math.random() * 255);

//   return `rgb(${r}, ${g}, ${b})`;
// }


// document.body.addEventListener('mousemove', () => {
//   for(let span of title1.children) {
//     span.style.color = getRandomColor();
//   }  
// })