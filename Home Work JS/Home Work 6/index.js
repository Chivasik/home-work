// Завдання
// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// 1) При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// 2) Створити метод getAge() який повертатиме скільки користувачеві років.
// 3) Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.





const createNewUser = () => {

	const firstName = prompt('Enter the first name');
	const lastName = prompt('Enter the last name');
	const birthday = prompt('Enter your birthday');

	 const newUser = {

		firstName,
		lastName,
		birthday,
		getLogin(){

			return firstName[0].toLowerCase() + lastName.toLowerCase();
		},

		getAge(){

			const now = new Date(); // создаю слепок времени на момен выполнения скрипта 
			let year = now.getFullYear();   //находим какой сейчас год 
			// найти год рождения пользователя 
			const dateBirthday =  birthday.slice(6);								
			console.log(dateBirthday);
			return year - dateBirthday;

		},

		getPassword(){
			let year = birthday.slice(6);
			let tmp = firstName[0].toUpperCase() + lastName.toLowerCase()+year;
			return tmp;
		}

	 }
	 return newUser;
	
}


const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getLogin());
console.log('password',newUser.getPassword());


