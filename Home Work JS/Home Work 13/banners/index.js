const imgContainer = document.getElementById("images-wrapper");
const images = Array.from(imgContainer.getElementsByClassName("image-to-show"));
const { btnPause, btnResume } = createBtn();

document.body.append(btnPause, btnResume);

let curentIndex = 0;
let interval;
images[curentIndex].style.display = "block";

function showNextImg() {
  btnPause.style.display = "block";
  btnResume.style.display = "none";

  images[curentIndex].style.display = "none";
  curentIndex = (curentIndex + 1) % images.length;
  images[curentIndex].style.display = "block";
}

interval = setInterval(showNextImg, 3000);

function createBtn() {
  const btnResume = document.createElement("button");
  const btnPause = document.createElement("button");
  btnPause.style.display = "none";
  btnResume.style.display = "none";

  btnResume.innerText = "Возобновить показ";
  btnPause.innerText = "Прекратить";

  btnResume.addEventListener("click", () => {
    if (!interval) {
      interval = setInterval(showNextImg, 3000);
      btnResume.style.display = "none";
      btnPause.style.display = "block";
    }
  });

  btnPause.addEventListener("click", () => {
    clearInterval(interval);
    interval = null;
    btnResume.style.display = "block";
    btnPause.style.display = "none";
  });

  return { btnPause, btnResume };
}
