// Завдання
// Реалізувати функцію підрахунку факторіалу числа. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера число, яке введе користувач.
// За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// Використовувати синтаксис ES6 для роботи зі змінними та функціями.
// Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів число, або при введенні вказав не число, - запитати число заново (при цьому значенням для нього повинна бути введена раніше інформація).

let n;
do {
  n = prompt("Enter the number");
} while (!isNumber(n));


function isNumber(value){

	//1 Преобразовать в число //2 проверить что полученное значение не NaN 
	const convertedValue = +value
	if (Number.isNaN(convertedValue)){

		return false
	}else{
		return true
	}

}
function factorial(n) {
  if (n < 0) {
    return -1;
  } else if (n === 0) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}
console.log(factorial(n));

// function pow(x,n){
// 		let result = 1
// 		for (let i = 0; i < n; i++){
// 			result*=x
// 		}
// 		return result

// 	}

// 	alert(pow(2,4))

// function pow(x,n){

// 	if(n==1){
// 		return x
// 	}else{
// 		return x*(pow(x, n-1))
// 	}
// 	//return x*(pow(x, n-1))
// }console.log(pow(2,3))
