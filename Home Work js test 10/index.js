// Завдання
// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:

// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.


const btnContainer = document.querySelector('.tabs')
const btnText = document.querySelectorAll('.tabs-content li')
console.log(btnContainer)
console.log(btnText)

btnContainer.addEventListener('click', (event) => {
	console.log(event.target.dataset)
	const btnTitle =  event.target.dataset.title
	console.log(btnTitle)
	const contetnElem = document.querySelector(`.tabs-content li[data-text="${btnTitle}"]`)
	console.log(contetnElem)
	const activeElement = document.querySelector('.tabs-content-active')
	console.log(activeElement)
	activeElement.classList.remove('tabs-content-active')
	contetnElem.classList.add('tabs-content-active')

	// const activeElemContainer = document.querySelector('.active')
	// console.log(activeElemContainer)
	// activeElemContainer.classList.remove('active')
	// console.log(activeElemContainer)
	
	// btnTitle.classList.add('active')

	
	
})